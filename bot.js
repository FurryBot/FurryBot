global.config = require("./config.js");
global.logger = require("./utility/logger.js");
global.r = require("rethinkdbdash")();
global.db = require("./utility/dbFunctions.js");
global.custom = Object.assign({}, require("./utility/misc.js"), require("./utility/functions.js"));
global.Discord = require("discord.js");
global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.truncate = require("truncate");
global.prettyyable = require("prettytable");
global._ = require("lodash");
global.fp = require("lodash/fp");
global.colors = require("console-colors-2");
global.fs = require("fs");
global.path = require("path");
global.Trello = require("trello");
global.os = require("os");
global.semver = require("semver");
global.util = require('util');
global.Trello = require("trello");
global.tclient = new Trello(config.trello.apiKey,config.trello.apiToken);
global.listStats = require("./utility/listStats.js");
global.Canvas = require("canvas-constructor").Canvas;
global.fsn = require("fs-nextra");
global.request = require("async-request");
global.fetch = require("node-fetch");
global.fileType = require("file-type");
global.messageCount = 0;
global.localMessageCount = 0;
global.furpile = {};
global.commandTimeout = new Set();
global.yiffDeprecationViewed = new Set();
global.dbErrorMessage = new Set();
global.download = function(uri, filename){
  return new Promise((resolve,reject)=>{
	  request(uri, function(err, res, body){

		request(uri).pipe(fsn.createWriteStream(filename)).on('close', resolve);
	  });
  });
};

// main client
global.client = new Discord.Client(config.client);

var file = `${config.rootDir}/counts/guilds_${config.currentDate}.txt`;
if(!fs.existsSync(file)) {
	fs.writeFile(file, "0", ((err) => {
		if(err) console.error(`Error setting up guild counts: ${err.message}`);
	}));
}

var xhr = new XMLHttpRequest();

xhr.addEventListener("readystatechange", function () {
	if (this.readyState === 4) {
		var t=JSON.parse(this.responseText);
		if(t.error) return console.error(`Error loading commands: ${t.error}`);
		config.commandList = t.return;
		
		config.commandCategories = {
			moderation: config.commandList.moderationCategory,
			fun: config.commandList.funCategory,
			info: config.commandList.infoCategory,
			misc: config.commandList.miscCategory,
			nsfw: config.commandList.nsfwCategory,
			utility: config.commandList.utilityCategory,
			economy: config.commandList.economyCategory,
			logging: config.commandList.loggingCategory,
			uncategorized: config.commandList.uncategorizedCategory,
			utility: config.commandList.utilityCategory
		};
		console.debug(`Command list loaded.`, client);
		for(var key in config.commandList.fullList) {
			commandTimeout[key]=new Set();
		}
		commandTimeout.f=new Set();
		commandTimeout.whatismyprefix=new Set();
		console.debug(`Command timeouts setup`);
    }
});

xhr.open("GET", "https://api.furrybot.me/commands");
xhr.setRequestHeader("Authorization", `Key ${config.apiKey}`);
xhr.send();

global.reloadCommands=(()=>{
	if(!XMLHttpRequest) var { XMLHttpRequest } = require("xmlhttprequest");
	var xhr = new XMLHttpRequest();

	xhr.addEventListener("readystatechange", function () {
	    if (this.readyState === 4) {
			config.commandList = JSON.parse(this.responseText).return;
			config.commandCategories = {
			"moderation": config.commandList.moderationCategory,
			"fun": config.commandList.funCategory,
			"info": config.commandList.infoCategory,
			"misc": config.commandList.miscCategory,
			"nsfw": config.commandList.nsfwCategory,
			"utility": config.commandList.utilityCategory,
			"economy": config.commandList.economyCategory,
			"logging": config.commandList.loggingCategory,
			"uncategorized": config.commandList.uncategorizedCategory,
			"utility": config.commandList.utilityCategory
		};
		console.debug(`Command list loaded Reloaded.`, client);
		for(var key in config.commandList.fullList) {
			commandTimeout[key]=new Set();
		}
		commandTimeout.f=new Set();
		commandTimeout.whatismyprefix=new Set();
		console.debug(`Command timeouts resetup`, client);
	  }
	});

	xhr.open("GET", "https://api.furrybot.me/commands");
	xhr.setRequestHeader("Authorization", "Key A19730225b");
	xhr.send();
	return true;
});
global.reloadCommandModules=(()=>{
	for(var key in require.cache){
		if(key.indexOf("\\node_modules") == -1){
			delete require.cache[key];
		}
	}
	return true;
});
global.reloadModules=(()=>{
	for(var key in require.cache){
		if(key.indexOf("\\node_modules") != -1){
			delete require.cache[key];
		}
	}
	return true;
});
global.reloadAll=(()=>{
	for(var key in require.cache){
		if(key.indexOf("\\node_modules") != -1){
			delete require.cache[key];
		}
	}
	return true;
});

global.stats = (async () => {
	var date = new Date();
	var d=`${date.getMonth()+1}-${date.getDate()}-${date.getFullYear()}`;
	if(config.currentDate != d) {
		var file=`./counts/guilds_${config.currentDate}.txt`;
		var newfile=`./counts/guilds_${d}.txt`;
		fs.readFile(file, "UTF8", ((err, data) => {
			if(err) console.error(err);
			console.log(`Total guilds joined today: ${data}\nTotal: ${client.guilds.size}`);
			var dt={
				author: {
					name: "Donovan_DMC#1337",
					"icon_url": "https://i.1.mcpro-games.mcprocdn.com/Don.gif"					
				},
				title: `Total Guilds Joined ${config.currentDate}\tTotal: ${client.guilds.size}`,
				description: `Total guilds joined today: **${data}**`,
				footer: {
					text: `Shard ${client.shard.id+1}/${client.shard.count} - Bot Version ${config.bot.version}`
				},
				color: custom.randomColor(),
				timestamp: custom.getCurrentTimestamp(),
				thumbnail: {
					url: "https://i.furrybot.me/furry-small.png"
				}
			};
			var embed=new Discord.RichEmbed(dt);
			client.channels.get(config.dailyGuildCountChannel).send(embed).catch(e=>{
				client.channels.get("475194067345473537").send(`[stats]: ${e.toString()}`);
			});
		}));
		
		fs.writeFile(newfile, "0", ((err) => {
			if(err) console.error(err);
		}));
		config.currentDate=d;
	}
	var s=await r.table("stats").get("generalStats").run();
	msgCount=localMessageCount+s.shards[client.shard.id].messageCount;
	await r.table("stats").get("generalStats").update({"shards":{[client.shard.id]:{"guildCount":client.guilds.size,"channelCount":client.channels.size,"userCount":client.users.size,"messageCount":msgCount,"shardID":client.shard.id,"shardCount":client.shard.count,"commandCount":config.commandList.all.length}}}).run();
	console.debug(`Posted Stats\nguildCount: ${client.guilds.size}\nchannelCount: ${client.channels.size}\nuserCount: ${client.users.size}\nmessageCount: ${messageCount}\nshardID: ${client.shard.id}\nshardCount: ${client.shard.count}`);
	localMessageCount=0;
	
	var n= await r.table("stats").get("generalStats");
	return n;
});

global.postGuildStats = (() => {
	return true;
});

var embed_defaults_na = {"footer": {text: `Shard ${client.shard.id+1}/${client.shard.count} - Bot Version ${config.bot.version}`}, "color": custom.randomColor(), "timestamp": custom.getCurrentTimestamp(),"thumbnail": {"url": "https://i.furrybot.me/furry-small.png"}};

client.on("ready", async() => {
	try{
		console.debug(`Bot has started with ${client.users.size} users in ${client.channels.size} channels of ${client.guilds.size} guilds.`);
		rotatePlaying = (()=>{
			return
			[
				client.user.setActivity(`🐾 f!help for help! 🐾`,{"type":"PLAYING"}),
				setTimeout(()=>{
					client.user.setActivity(`🐾 f!help in ${client.guilds.size} Guilds! 🐾`,{"type":"WATCHING"});
				}, 7e3),
				setTimeout(()=>{
					client.user.setActivity(`🐾 f!help in ${client.channels.size} Channels! 🐾`,{"type":"LISTENING"});
				}, 14e3),
				setTimeout(()=>{
					client.user.setActivity(`🐾 f!help With ${client.users.size} Users! 🐾`,{"type":"PLAYING"});
				}, 21e3),
				setTimeout(()=>{
					client.user.setActivity(`🐾 f!help On Shard ${client.shard?(client.shard.id+1)+"/"+client.shard.count:"1/1"} 🐾`,{"type":"PLAYING"});
				}, 28e3)
			];
		});
		
		rotatePlaying();
		setInterval(rotatePlaying, 35e3);
		
		if(!config.beta) {
			stats();
			setInterval(stats,3e5);
			
			postGuildStats();
			setInterval(postGuildStats,9e5);
		} else {
			console.debug("Not posting stats from beta instance");
		}
	}catch(e) {
		console.error(`[readyEvent][Guild: N/A]: ${e}`);
	}
});

client.on("guildCreate", async (guild) => {
	try {
		if(!guild) return;
		require("./eventhandlers/guildCreate.js")(guild);
	}catch(e){
		console.error(`[guildCreateEvent][Guild: ${guild.id}]: ${e}`);
	}
});

client.on("guildDelete", async(guild) => {
	try {
		if(!guild) return;
		require("./eventhandlers/guildDelete.js")(guild);
	}catch(e){
		console.error(`[guildDeleteEvent][Guild: ${guild.id}]: ${e}`);
	}
});

client.on("channelCreate", async (channel) => {
	try{
		if(!channel) return;
		var d=new Date();
		if(d.getTime() - 3e5 < channel.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(channel.guild.id);
		if(gConfig.logging.channelcreated.enabled) {
			require("./eventhandlers/channelCreate.js")(channel,gConfig);
		}
	}catch(e){
		console.error(`[channelCreateEvent][Guild: ${channel.guild.id}]: ${e}`);
	}
});

client.on("channelDelete", async (channel) => {
	try {
		if(!channel) return;
		var d=new Date();
		if(d.getTime() - 3e5 < channel.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(channel.guild.id);
		if(gConfig.logging.channeldestroyed.enabled) {
			require("./eventhandlers/channelDelete.js")(channel,gConfig);
		}
	}catch(e){
		console.error(`[channelDeleteEvent][Guild: ${channel.guild.id}]: ${e}`);
	}
});

client.on("channelUpdate", async (oldChannel,newChannel) => {
	try{
		if(!oldChannel || !newChannel) return;
		var d=new Date();
		if(d.getTime() - 3e5 < oldChannel.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(newChannel.guild.id);
		if(gConfig.logging.channelupdated.enabled) {
			require("./eventhandlers/channelUpdate.js")(oldChannel,newChannel,gConfig);
		}
	}catch(e){
		console.error(`[channelUpdateEvent][Guild: ${oldChannel.guild.id}]: ${e}`);
	}
});

client.on("guildBanAdd", async (guild,user) => {
	try {
		if(!guild || !user) return;
		var d=new Date();
		if(d.getTime() - 3e5 < guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(guild.id);
		if(gConfig.logging.userbanned.enabled) {
			require("./eventhandlers/guildBanAdd.js")(guild,user,gConfig);
		}
	}catch(e){
		console.error(`[guildBanAddEvent][Guild: ${guild.id}]: ${e}`);
	}
});

client.on("guildBanRemove", async (guild,user) => {
	try {
		if(!guild || !user) return;
		var d=new Date();
		if(d.getTime() - 3e5 < guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(guild.id);
		if(gConfig.logging.userunbanned.enabled) {
			require("./eventhandlers/guildBanRemove.js")(guild,user,gConfig);
		}
	}catch(e){
		console.error(`[guildBanRemoveEvent][Guild: ${guild.id}]: ${e}`);
	}
});

client.on("guildMemberAdd", async (member) => {
	if(!member && member.id !== client.user.id) return;
	var d=new Date();
		if(d.getTime() - 3e5 < member.guild.me.joinedTimestamp) return;
	var gConfig=await db.getGuild(member.guild.id);
	if(gConfig.logging.join.enabled) {
	    require("./eventhandlers/guildMemberAdd.js")(member,gConfig);
	}
});

client.on("guildMemberRemove", async (member) => {
	try {
		if(!member && member.id !== client.user.id) return;
		var d=new Date();
		if(d.getTime() - 3e5 < member.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(member.guild.id);
		if(gConfig.logging.leave.enabled) {
			require("./eventhandlers/guildMemberRemove.js")(member,gConfig);
		}
	}catch(e){
		console.error(`[guildMemberRemoveEvent][Guild: ${member.guild.id}]: ${e}`);
	}
});

client.on("guildMemberUpdate", async (oldMember,newMember) => {
	try {
		if(!oldMember || !newMember) return;
		var d=new Date();
		if(d.getTime() - 3e5 < oldMember.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(newMember.guild.id);
		if(gConfig.logging.userupdated.enabled) {
			require("./eventhandlers/guildMemberUpdate.js")(oldMember,newMember,gConfig);
		}
	}catch(e){
		console.error(`[guildMemberUpdateEvent][Guild: ${oldMember.guild.id}]: ${e}`);
	}
});

client.on("userUpdate", async (oldUser,newUser) => {
	try {
		if(!oldUser || !newUser) return;
		var guilds=await client.guilds.filter((guild) => {
			if(guild.members.has(newUser.id)){
				return true;
			}else{
				return false;
			}
		});
		guilds.forEach(async (guild) => {
			var gConfig=await db.getGuild(guild.id);
			if(gConfig.logging.userupdated.enabled) {
				require("./eventhandlers/userUpdate.js")(oldUser,newUser,gConfig,guild);
			}
		});
	}catch(e){
		console.error(`[userUpdateEvent][Guild: N/A]: ${e}`);
	}
});

client.on("guildUpdate", async (oldGuild,newGuild) => {
	try{
		if(!oldGuild || !newGuild) return;
		var d=new Date();
		if(d.getTime() - 3e5 < oldGuild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(newGuild.id);
		if(gConfig.logging.guildupdated.enabled) {
			require("./eventhandlers/guildUpdated.js")(oldGuild,newGuild,gConfig);
		}
	}catch(e){
		console.error(`[guildUpdateEvent][Guild: ${oldGuild.guild.id}]: ${e}`);
	}
});

client.on("roleCreated", async (role) => {
	try {
		if(!role) return;
		var d=new Date();
		if(d.getTime() - 3e5 < role.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(role.guild.id);
		if(gConfig.logging.rolecreated.enabled) {
			require("./eventhandlers/roleCreated.js")(role,gConfig);
		}
	}catch(e){
		console.error(`[roleCreatedEvent][Guild: ${role.guild.id}]: ${e}`);
	}
});

client.on("roleDelete", async (role) => {
	try {
		if(!role) return;
		var d=new Date();
		if(d.getTime() - 3e5 < role.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(role.guild.id);
		if(gConfig.logging.roledeleted.enabled) {
			require("./eventhandlers/roleDelete.js")(role,gConfig);
		}
	}catch(e){
		console.error(`[roleDeleteEvent][Guild: ${role.guild.id}]: ${e}`);
	}
});

client.on("roleUpdate", async (oldRole,newRole) => {
	try {
		if(!oldRole || !newRole) return;
		var d=new Date();
		if(d.getTime() - 3e5 < oldRole.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(newRole.guild.id);
		if(gConfig.logging.roleupdated.enabled) {
			require("./eventhandlers/roleUpdate.js")(oldRole,newRole,gConfig);
		}
	}catch(e){
		console.error(`[roleUpdateEvent][Guild: ${newRole.guild.id}]: ${e}`);
	}
});

client.on("messageDelete", async (message) => {
	try{
		if(!message || (!message.content ^ !message.attachments)) return;
		var d=new Date();
		if(d.getTime() - 3e5 < message.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(message.guild.id);
		if(gConfig.logging.messagedeleted.enabled) {
			require("./eventhandlers/messageDelete.js")(message,gConfig);
		}
	}catch(e){
		console.error(`[messageDeleteEvent][Guild: ${message.guild.id}]: ${e}`);
	}
});

client.on("messageUpdate", async (oldMessage,newMessage) => {
	try{
		if((!oldMessage || (!oldMessage.content ^ !oldMessage.attachments)) || (!newMessage || (!newMessage.content ^ !newMessage.attachments))) return;
		var d=new Date();
		if(d.getTime() - 3e5 < oldMessage.guild.me.joinedTimestamp) return;
		var gConfig=await db.getGuild(newMessage.guild.id);
		if(gConfig.logging.messageupdated.enabled) {
			require("./eventhandlers/messageUpdate.js")(oldMessage,newMessage,gConfig);
		}
	}catch(e){
		console.error(`[messageUpdateEvent][Guild: ${oldMessage.guild.id}]: ${e}`);
	}
});

client.on("message", async (message) => {
	try {
		
		messageCount++;
		localMessageCount++;
		
		if(message.author.bot) return;
		
		var embed_defaults = {"footer": {text: `Shard ${client.shard.id+1}/${client.shard.count} - Bot Version ${config.bot.version}`}, "author": {"name": message.author.tag,"icon_url": message.author.avatarURL}, "color": custom.randomColor(), "timestamp": custom.getCurrentTimestamp()};
		
		try {
			const gConfig = await db.getGuild(message.guild.id) ||  config.bot.guildDefaultConfig;
		}catch(e){
			console.error(e);
			return;
		}

		var prefix = message.content.startsWith(`<@${client.user.id}>`)?`<@${client.user.id}>`:message.content.startsWith(`<@!${client.user.id}>`)?`<@!${client.user.id}>`:config.beta?"fb!":gConfig.prefix;
		global.ar=message.content.slice(prefix.length).trim().split(/\s+/g);
		var args = [...ar];
		var command = args.shift().toLowerCase();
		
		if(message.guild.id == "400284026885373962") {
			if(config.customCommands.includes(command)) {
				console.commandlog(`Command  "${command}" ran with arguments "${args.join(" ")}" by user ${message.author.tag} (${message.author.id}) in guild ${message.guild.name} (${message.guild.id})`, client);
			
			switch(command) {
				case "updates":
					if(message.member.roles.some(r=>["449444946269700096"].includes(r.id))) {
						message.member.removeRole("449444946269700096");
						return message.reply("You will no longer get notified when announcements happen\n(you're missing out! run this command again to start getting notified again)");
					} else {
						message.member.addRole("449444946269700096");
						return message.reply("You will be notified when announcements happen\n(run the command again to not be notified)");
					}
					break;
			}
			return;
			}
		}
		
		if(message.content.toLowerCase() == "whatismyprefix") {
			if(commandTimeout.whatismyprefix.has(message.author.id) && !config.developers.includes(message.author.id)) {
				console.log(`Command timeout encountered by user ${message.author.tag} (${message.author.id}) on response "whatismyprefix" in guild ${message.guild.name} (${message.guild.id})`, client);
				return message.reply(`${config.emojis.cooldown}\nPlease wait ${custom.ms(config.commandList.response.whatismyprefix.cooldown)} before using this again!`);
			}
				commandTimeout.whatismyprefix.add(message.author.id);
				setTimeout(() => {commandTimeout.whatismyprefix.delete(message.author.id);}, config.commandList.response.whatismyprefix.cooldown);
			var n=await r.table("stats").get("commandStats").run();
			var tbl = r.table("stats").get("commandStats");
			if(!n.total.whatismyprefix) {
					tbl.update({total:{whatismyprefix:1}}).run();
				} else {
					var l=n.total.whatismyprefix;
					tbl.update({total:{whatismyprefix:parseInt(l)+1}}).run();
				}
			console.commandlog(`Response of "whatismyprefix" triggered by user ${message.author.tag} (${message.author.id}) in guild ${message.guild.name} (${message.guild.id})`, client);
			return message.reply(`this guilds prefix is **${gConfig.prefix}**`);
		}
		
		if(message.content.toLowerCase() == "f") {
			if(gConfig.fResponseEnabled) {
				if(commandTimeout.f.has(message.author.id) && !config.developers.includes(message.author.id)) {
					console.log(`Command timeout encountered by user ${message.author.tag} (${message.author.id}) on response "f" in guild ${message.guild.name} (${message.guild.id})`, client);
					return message.reply(`${config.emojis.cooldown}\nPlease wait ${custom.ms(config.commandList.response.f.cooldown)} before using this again!`);
				}
				commandTimeout.f.add(message.author.id);
				setTimeout(() => {commandTimeout.f.delete(message.author.id);}, config.commandList.response.f.cooldown);
				var n=await r.table("stats").get("commandStats").run();
				var tbl = r.table("stats").get("commandStats");
				if(!n.total.f) {
					tbl.update({total:{f:1}}).run();
				} else {
					var l=n.total.f;
					tbl.update({total:{f:parseInt(l)+1}}).run();
				}
				var f = await r.table("stats").get("fCount");
				r.table("stats").get("fCount").update({count:parseInt(f.count)+1}).run();
				console.commandlog(`Response of "f" triggered by user ${message.author.tag} (${message.author.id}) in guild ${message.guild.name} (${message.guild.id})`, client);
				return message.channel.send(`<@!${message.author.id}> has paid respects.\n\nRespects paid total: ${parseInt(f.count)+1}`);
			}
		}
		
		try {
			if(message.content === `<@${client.user.id}>` || message.content === `<@!${client.user.id}>`) {
				var c=await require(`./commands/${config.commandList.fullList["help"].category}/help-cmd.js`)(message,gConfig);
				if(c instanceof Error) throw c;
			}
		}catch(e){
			message.reply(`Error while running command: ${e}`);
			return console.error(`[messageEvent][Guild: ${message.guild.id}]: Command error:\n\tCommand: ${command}\n\tSupplied arguments: ${args.join(' ')}\n\tServer ID: ${message.guild.id}\n\t${e.stack}`);
		}
		if(!message.content.startsWith(prefix)) return;
		
		if(!config.commandList.all.includes(command)) return;
		
		if(gConfig.deleteCmds)  message.delete().catch(error=>{ message.channel.send(`Unable to delete command invocation: **${error}**\n\nCheck my permissions.`); });
		
		if(config.commandList.fullList[command].userPermissions.length > 0) {
			if(config.commandList.fullList[command].userPermissions.some(perm => !message.channel.permissionsFor(message.member).has(perm,null,true,true))) {
				var neededPerms = config.commandList.fullList[command].userPermissions.filter(perm => !message.channel.permissionsFor(message.member).has(perm,null,true,true));
				for(i=0;i<neededPerms.length;i++) {
					var n=await r.table("stats").get("missingPermissions").run();
					r.table("stats").get("missingPermissions").update({user:{[neededPerms[i]]:[parseInt(n.user[neededPerms[i]])+1]}}).run();
				}
				var neededPerms = neededPerms.length > 1 ? neededPerms.join(", ") : neededPerms;
				var data={
						"title": "You Don't have permission to do this!",
						"description": `You require the permission(s) **${neededPerms}** to run this command!`
				};
				Object.assign(data, embed_defaults);
				var embed = new Discord.RichEmbed(data);
				console.debug(`User ${message.author.tag} (${message.author.id}) is missing the permission(s) ${neededPerms} to run the command ${command} in guild ${message.guild.name} (${message.guild.id})`);
				return message.channel.send(embed);
			}
		}
		
		
		if(config.commandList.fullList[command].botPermissions.length > 0) {
			if(config.commandList.fullList[command].botPermissions.some(perm => !message.channel.permissionsFor(message.guild.me).has(perm,null,true,true))) {
				var neededPerms = config.commandList.fullList[command].botPermissions.filter(perm => !message.channel.permissionsFor(message.guild.me).has(perm,null,true,true));
				for(i=0;i<neededPerms.length;i++) {
					var n=await r.table("stats").get("missingPermissions").run();
					r.table("stats").get("missingPermissions").update({client:{[neededPerms[i]]:[parseInt(n.client[neededPerms[i]])+1]}}).run();
				}
				var neededPerms = neededPerms.length > 1 ? neededPerms.join(", ") : neededPerms;
				var data={
					"title": "I don't have the required permissions!",
					"description": `I require the permission(s) **${neededPerms}** to run this command!`
				};
				Object.assign(data, embed_defaults);
				var embed = new Discord.RichEmbed(data);
				console.debug(`I am missing the permission(s) ${neededPerms} to run the command ${command} in guild ${message.guild.name} (${message.guild.id})`);
				return message.channel.send(embed);
			}
		}
		
		if(config.commandList.fullList[command].nsfw === true && !message.channel.nsfw) {
			var data={
				"title": "NSFW commands are not allowed here",
				"description": "NSFW commands must be ran in channels marked as NSFW"
			};
			Object.assign(data, embed_defaults);
			var embed = new Discord.RichEmbed(data);
			return message.channel.send(embed);
		}
		
		var isAlias=(config.commandList.fullList[command].alias == "true");
		var n=await r.table("stats").get("commandStats").run();
		var tbl = r.table("stats").get("commandStats");
		try {
		switch(isAlias) {
			case true:
				//alias
				if(commandTimeout[config.commandList.fullList[command].aliasof].has(message.author.id) && !config.developers.includes(message.author.id)) {
					console.log(`Command timeout encountered by user ${message.author.tag} (${message.author.id}) on command alias "${command}" (aliasof: ${config.commandList.fullList[command].aliasof}) in guild ${message.guild.name} (${message.guild.id})`, client);
					return message.reply(`${config.emojis.cooldown}\nPlease wait ${custom.ms(config.commandList.fullList[command].cooldown)} before using this command again!`);
					}
				commandTimeout[config.commandList.fullList[command].aliasof].add(message.author.id);
				setTimeout(() => {commandTimeout[config.commandList.fullList[command].aliasof].delete(message.author.id);}, config.commandList.fullList[command].cooldown);
				if(!n.aliases[command]) {
					tbl.update({aliases:{[command]:1}}).run();
				} else {
					var l=n.aliases[command];
					tbl.update({aliases:{[command]:parseInt(l)+1}}).run();
				}
				
				if(!n.total[config.commandList.fullList[command].aliasof]) {
					tbl.update({total:{[config.commandList.fullList[command].aliasof]:1}}).run();
				} else {
					var l=n.total[config.commandList.fullList[command].aliasof];
					tbl.update({total:{[config.commandList.fullList[command].aliasof]:parseInt(l)+1}}).run();
				}
				console.commandlog(`Command Alias "${command}" (aliasof: ${config.commandList.fullList[command].aliasof}) ran with arguments "${args.join(" ")}" by user ${message.author.tag} (${message.author.id}) in guild ${message.guild.name} (${message.guild.id})`, client);
				var c=await require(`./commands/${config.commandList.fullList[command].category}/${config.commandList.fullList[command].aliasof}-cmd.js`)(message,gConfig);
				if(c instanceof Error) throw c;
				break;
				
			default:
				if(commandTimeout[command].has(message.author.id) && !config.developers.includes(message.author.id)) {
					console.log(`Command timeout encountered by user ${message.author.tag} (${message.author.id}) on command "${command}" in guild ${message.guild.name} (${message.guild.id})`, client);
					return message.reply(`${config.emojis.cooldown}\nPlease wait ${custom.ms(config.commandList.fullList[command].cooldown)} before using this command again!`);
				}
				commandTimeout[command].add(message.author.id);
				setTimeout(() => {commandTimeout[command].delete(message.author.id);}, config.commandList.fullList[command].cooldown);
				if(!n.main[command]) {
				tbl.update({main:{[command]:1}}).run();
				} else {
					var l=n.main[command];
					tbl.update({main:{[command]:parseInt(l)+1}}).run();
				}
			
				if(!n.total[command]) {
					tbl.update({total:{[command]:1}}).run();
				} else {
					var l=n.total[command];
					tbl.update({total:{[command]:parseInt(l)+1}}).run();
				}
				console.commandlog(`Command  "${command}" ran with arguments "${args.join(" ")}" by user ${message.author.tag} (${message.author.id}) in guild ${message.guild.name} (${message.guild.id})`, client);
				var c=await require(`./commands/${config.commandList.fullList[command].category}/${command}-cmd.js`)(message,gConfig);
				if(c instanceof Error) throw c;
		}
	}catch(e){
		if(e.message == "INVALID_USAGE") {
			var isAlias=(config.commandList.fullList[command].alias == "true");
			var cmd=isAlias?config.commandList.fullList[command].aliasof:command;
			var usage=config.commandList.fullList[cmd].usage;
			var data={
				title: ":x: Invalid Command Usage",
				fields: [
					{
						name: "Command",
						value: cmd,
						inline: false
					},{
						name: "Usage",
						value: usage,
						inline: false
					},{
						name: "Arguments Provided",
						value: args.join(" ")||"NONE",
						inline: false
					}
				]
			};
			Object.assign(data, embed_defaults);
			var embed=new Discord.RichEmbed(data);
			return message.channel.send(embed)
		} else {
			message.reply(`Error while running command: ${e}`);
			return console.error(`[messageEvent][Guild: ${message.guild.id}]: Command error:\n\tCommand: ${command}\n\tSupplied arguments: ${args.join(' ')}\n\tServer ID: ${message.guild.id}\n\t${e.stack}`);
		}
	}
	}catch(e){
		message.reply(`Error while running command: ${e}`);
		return console.error(`[messageEvent][Guild: ${message.guild.id}]: Command error:\n\tCommand: ${command}\n\tSupplied arguments: ${args.join(' ')}\n\tServer ID: ${message.guild.id}\n\t${e.stack}`);
	}
});

client.on("error", async (error) => {
	console.error(error, client);
});

client.on("debug", async (info) => {
	//console.debug(info, client);
	// debug spams too much
});

client.on("warn", async (info) => {
	console.warn(info, client);
});

if(!config.beta) {
	console.log(`[ClientLogin] Launching normal bot..`);
	client.login(config.bot.token);
} else {
	console.warn(`[ClientLogin] Warning! Launching beta version of bot.. (waiting ${.5e3/1000} seconds)${colors.sp.reset}`);
	setTimeout(function(token){
		client.login(token);
	}, .5e3, config.bot.token);
}

process.on("SIGINT", async () => {
	client.destroy();
	console.debug(`${colors.fg.red}${colors.sp.bold}Force close via CTRL+C${colors.sp.reset}`, client);
	//global.twitterStream.destroy();
	process.kill(process.pid, 'SIGTERM' );
});