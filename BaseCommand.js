module.exports=(async (message,gConfig) => {
	if(!message) return new Error ("missing message parameter");
	if(!gConfig) return new Error ("missing gConfig parameter");
	/*if(!furpile_timeout) {
		global.furpile_timeout={};
	}
	global.config = require("./config.js");
	global.logger = require("./utility/logger.js");
	global.db = require("./utility/dbFunctions.js");
	global.r = require("./db.js");
	global.custom = Object.assign({}, require("./utility/misc.js"), require("./utility/functions.js"));
	global.Discord = require("discord.js");
	global.Twitter = require("twitter");
	// unaware of how to destructure when making global variables
	global.XMLHttpRequest= require("xmlhttprequest").XMLHttpRequest;
	global.convert = require("xml-js");
	global.truncate = require("truncate");
	global.prettyyable = require("prettytable");
	global._ = require("lodash");
	global.fp = require("lodash/fp");
	global.colors = require("console-colors-2");
	global.fs = require("fs");
	global.DBL = require("dblapi.js");
	global.Trello = require("trello");
	global.os = require("os");
	global.semver = require("semver");
	global.util = require('util');
	global.truncate = require("truncate");*/
	//global.memwatch = require("memwatch-next");
	global.args = message.content.slice(gConfig.prefix.length).trim().split(/\s+/g);
	global.command = args.shift().toLowerCase();
	global.embed_defaults = {"footer": {text: `Shard ${client.shard.id+1}/${client.shard.count} - Bot Version ${config.bot.version}`}, "author": {"name": message.author.tag,"icon_url": message.author.avatarURL}, "color": global.custom.randomColor(), "timestamp": global.custom.getCurrentTimestamp(),"thumbnail": {"url": "https://i.furrybot.me/furry-small.png"}};
	global.embed_defaults_na = {"footer": {text: `Shard ${client.shard.id+1}/${client.shard.count} - Bot Version ${config.bot.version}`}, "color": custom.randomColor(), "timestamp": custom.getCurrentTimestamp(),"thumbnail": {"url": "https://i.furrybot.me/furry-small.png"}};
});