functions={};

functions.createGuild = async function createGuild(guildID) {
	if(!client.guilds.has(guildID)) {
		console.log(`[dbFunctions][createGuild] Attempted to add guild to database that was not in bot, ${guildID}`,client);
		return new Error("GUILD_NOT_FOUND");
	}
	console.log(`Added guild ${guildID} with default configuration.`,client);
	var gConf = {id: guildID};
	Object.assign(gConf, config.bot.guildDefaultConfig);
	await r.table("guilds").insert(gConf);
	  return this.getGuild(guildID);
};
	
functions.getGuild = async function getGuild(guildID) {
    var guild=await r.table("guilds").get(guildID);
	if(!guild) {
		return this.createGuild(guildID);
	}
	
	return guild;
};
  
functions.updateGuild = async function updateGuild(guildID, fields) {
	return r.table("guilds").get(guildID).update(fields);
};
  
functions.addWarning = async function addWarning(guildID,userID,reason) {
	if(!client.guilds.has(guildID)) {
		console.log(`[dbFunctions][addWarning] Attempted to add warning to database for a guild that the bot is not in, ${guildID}`,client);
		return new Error("GUILD_NOT_FOUND");
	}
	return r.table("warnings").insert({guildID,userID,reason});
};

functions.removeWarning = async function deleteWarning(guildID,warningID) {
	if(!client.guilds.has(guildID)) return console.log(`[dbFunctions.js:deleteWarning]Attempted to remove warning from database in a guild that the bot is not in, ${guildID}`,client);
	if(!r.table("warnings").getAll(warningID).count().eq(1)) return new Error("RECORD_NOT_FOUND");
	if(!r.table("warnings").get(warningID).getField("guildID").eq(guildID)) return new Error("WRONG_GUILD");
	return r.table("warnings").get(warningID).delete();
};

functions.deleteUserWarnings = async function deleteUserWarnings(guildID,userID) {
	if(!client.guilds.has(guildID)) {
		console.log(`[dbFunctions][deleteUserWarnings]Attempted to remove warning from database in a guild that the bot is not in, ${guildID}`,client);
		return new Error("GUILD_NOT_FOUND");
	}
	if(r.table("warnings").filter({guildID,userID}).count().eq(0)) return new Error("NONE_FOUND");
	return r.table("warnings").filter({guildID,userID}).forEach((warning) => {
		return r.table("warnings").get(warning("id")).delete();
	});
};

functions.deleteGuild = async function deleteGuild(guildID) {
	  return r.table("guilds").get(guildID).delete();
};
  
functions.addBlock = async function addBlock (id) {
    return r.table("blocked").insert({ id });
};
  
functions.isBlocked = async function isBlocked (guildID, authorID = 1) {
    const res = await r.table("blocked").get(guildID) ||
                await r.table("blocked").get(authorID);

    return Boolean(res);
};

functions.addDonor = async function addDonor (id, donorAmount) {
    return r.table('donors')
      .insert({ id, donorAmount }, { conflict: 'update' })
      ;
};

functions.removeDonor = async function removeDonor (id) {
    return r.table('donors')
      .get(id)
      .delete()
      ;
};

functions.isDonor = async function isDonor (id) {
    const res = await r.table('donors')
      .get(id)
      ;
    return res ? res.donorAmount : false;
};
  
functions.getStats = async function getStats () {
    const res = await r.table('stats')
      .get(1)
      ;
    return res.stats;
};

functions.getFCount = async function getFCount () {
    const res = await r.table("f").get(1);
    return res.count;
};

functions.increaseFCount = async function increaseFCount () {
    var res = await r.table("f").get(1);
    var rs=res.count+1;
	var re = await r.table("f").get(1).update({count: rs});
	return re;
};

module.exports=functions;