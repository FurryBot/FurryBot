module.exports=(async (message, gConfig) => {
	if(!message) return new Error ("missing message parameter");
	if(!gConfig) return new Error ("missing gConfig parameter");
	await require(`../../BaseCommand.js`)(message, gConfig);
	if(!args[0]) {
		var lnk=gConfig.prefix != "f!"?`${config.documentationURL}?prefix=${gConfig.prefix}`:config.documentationURL;
		return message.channel.send(`You can view our full command documentation here: ${lnk}\n\nMake sure to checkout and vote for our bot on these lists:\n<${config.discordbotsURL}>\n<${config.listcordURL}>\n<${config.botsfordiscordURL}>\n\nMake sure to check the Trello board regularly: <${config.trelloURL}>\nYou can use **${gConfig.prefix}help <command>** to get help with a specific command.\nMake sure to check out our official Twitter account: ${config.twitter.accountURL}.\n\nJoin can join our support server here: ${config.discordSupportInvite}`);
	}
	
	if(config.commandList.all.indexOf(args[0]) === -1) {
		return message.reply("Invalid command provided");
	}
	
	var command=config.commandList.fullList[args[0]];
	cd=custom.ms(command.cooldown);
	var data={
		title: "Command Help",
		description: `Command: **${args[0]}**`,
		fields: [
		{
			name: "Usage",
			value: `${gConfig.prefix}${command.usage}`,
			inline: true
		},{
			name: "Description",
			value: command.description,
			inline: true
		},{
			name: "Bot Permissions",
			value: command.botPermissions.join(", ")||"NONE",
			inline: true
		},{
			name: "User Permissions",
			value: command.userPermissions.join(", ")||"NONE",
			inline: true
		},{
			name: "NSFW",
			value: custom.ucwords(command.nsfw),
			inline: true
		},{
			name: "Developer Only",
			value: custom.ucwords(command.devOnly),
			inline: true
		},{
			name: "Guild Owner Only",
			value: custom.ucwords(command.guildOwnerOnly),
			inline: true
		},{
			name: "Cooldown",
			value: custom.ucwords(cooldown),
			inline: true
		}
		]
	};
	
	Object.assign(data,embed_defaults);
	var embed=new Discord.RichEmbed(data);
	return message.channel.send(embed);
});